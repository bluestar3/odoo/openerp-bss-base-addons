import setuptools

with open('VERSION.txt', 'r') as f:
    version = f.read().strip()

setuptools.setup(
    name="odoo10-addons-bss-base",
    description="Meta package for bss-base Odoo addons",
    version=version,
    install_requires=[
        'odoo10-addon-bss_backend_theme',
        'odoo10-addon-bss_one2many_action',
        'odoo10-addon-bss_queue',
        'odoo10-addon-bss_utils',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Odoo',
    ]
)
